

const express = require('express');
require('./src/database/mongoose')
const userRouter = require('./src/routers/user')
const Taskrouter = require('./src/routers/task')
const app = express();



const port = process.env.PORT


app.use(express.json())



app.use(userRouter)
app.use(Taskrouter)



app.listen(port,()=>
{
    console.log(`server started on port ${port}`);
})

